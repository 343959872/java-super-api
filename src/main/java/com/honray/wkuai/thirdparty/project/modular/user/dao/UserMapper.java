package com.honray.wkuai.thirdparty.project.modular.user.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.honray.wkuai.thirdparty.project.modular.user.model.User;

/**
 * @title 用户DAOMapper接口
 * @author 曹海兵
 */
public interface UserMapper extends BaseMapper<User> {


}