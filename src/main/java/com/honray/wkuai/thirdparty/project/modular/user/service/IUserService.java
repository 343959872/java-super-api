package com.honray.wkuai.thirdparty.project.modular.user.service;

import com.baomidou.mybatisplus.service.IService;
import com.honray.wkuai.thirdparty.project.modular.user.model.User;

/**
 * @title 用户业务逻辑接口类
 *
 * @author caohaibing
 */
public interface IUserService extends IService<User> {


}
