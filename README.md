在此之前，我们假设您已经：

- 拥有 Java 开发环境以及相应 IDE
- 熟悉 Spring Boot
- 熟悉 Maven


## 一、本地搭建及运行

### 1.1、项目下载

项目git地址：https://gitee.com/-/ide/project/343959872/java-super-api.git

下载方式：

- 本地使用git下载：git clone https://gitee.com/-/ide/project/343959872/java-super-api.git
- 在项目git地址点击下载按钮进行下载
  ![image.png](https://oaapi.gdhonray.com/uploads/common/20181114/da022482365f609dcbe00d4cd4e7dd5e.png)
- 注意：框架中有一个用户信息的增删查改功能代码作为示例，真正开发中可以清理掉

### 1.2、项目导入IDEA

```
进入IDEA主界面，点击open，并选择下载好的java-super-api代码的根目录 
```

![image.png](https://oaapi.gdhonray.com/uploads/common/20181114/38584ac02965337c19798586ef7465d2.png)

![image.png](https://oaapi.gdhonray.com/uploads/common/20181114/38d4a1d1009b7f8137c453d9cf302142.png)



### 1.3、运行项目

运行前的准备:

> - 安装mysql数据库，作者所用mysql版本为5.7



1. 执行`db`目录下的`java-super-api.sql` 脚本，初始化测试运行的数据库环境 ;
2. 打开`java-super-api/src/main/resources/application.yml `配置文件，修改数据连接 ，账号 和 密码 ，改为您所连接数据库的配置,local为本地开发环境；
   ![image.png](https://oaapi.gdhonray.com/uploads/common/20181114/ac3ad573482547431080f1d9bcca7106.png)

1. 如需修改服务器端口或者context-path，默认的context-path为/，可参考下图 ；
   ![image.png](https://oaapi.gdhonray.com/uploads/common/20181114/d48c7e2eed45471411d07faaa745bd9c.png)
2. 执行`RestApplication`类中的main方法，即可运行基础框架； 
3. 打开浏览器，输入http://localhost:8080/user/list，即可访问到基础框架测试代码的响应数据；
    ![image.png](https://oaapi.gdhonray.com/uploads/common/20181121/9210f647ecf221987cbbb011f680151d.png)