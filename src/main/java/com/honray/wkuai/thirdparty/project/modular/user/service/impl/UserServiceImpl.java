package com.honray.wkuai.thirdparty.project.modular.user.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.honray.wkuai.thirdparty.project.modular.user.dao.UserMapper;
import com.honray.wkuai.thirdparty.project.modular.user.model.User;
import com.honray.wkuai.thirdparty.project.modular.user.service.IUserService;
import com.stylefeng.guns.core.exception.GunsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @title 用户服务
 * @author caohaibing
 */
@Service
@Transactional(rollbackFor = GunsException.class)
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
