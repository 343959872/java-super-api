package com.honray.wkuai.thirdparty.project.modular.user.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.honray.wkuai.common.base.ResponseTip;
import com.honray.wkuai.thirdparty.project.modular.user.model.User;
import com.honray.wkuai.thirdparty.project.modular.user.service.IUserService;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @title 用户管理控制类
 * @author caohaibing
 * @Date 2017年2月17日20:27:22
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private IUserService userService;

    /**
     * @title  获取所有用户列表
     */
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public Object list() {
        List<User> list = this.userService.selectList(null);
        return ResponseTip.success(list);
    }

    /**
     * @title 用户详情
     * @param Long userId 用户编号
     */
    @RequestMapping(value = "/detail",method = RequestMethod.GET)
    @ResponseBody
    public Object detail(Long userId) {
        return ResponseTip.success(userService.selectById(userId));
    }


    /**
     * @title 用户保存或者修改
     * @param Long userId 用户编号
     * @param String name 用户姓名
     * @param Integer age 用户年龄
     * @param String email 用户邮箱
     */
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @ResponseBody
    public Object save(Long userId,String name,Integer age,String email) {
        User user = new User();
        user.setId(userId);
        user.setName(name);
        user.setAge(age);
        user.setEmail(email);
        return ResponseTip.success(userService.insertOrUpdate(user));
    }

}
